package projeto_ia;

import java.util.ArrayList;
import java.util.Arrays;

public class Projeto_ia {

    public static void main(String[] args) {
        int[] estadoInicial = {1, 2, 3, 4, 0, 8, 5, 7, 6};
        int[] estadoSolucao = {1, 2, 3, 4, 5, 6, 7, 8, 0};

        No noInicial = new No(estadoInicial);
        No pai = moverZero(noInicial, estadoSolucao);

        while (pai.pai != null) {
            imprimir(pai.getEstado());
            System.out.println("");
            pai = pai.pai;

        }

    }

    public static No moverZero(No noInicial, int[] estadoSolucao) {
        ArrayList<No> filaExpandidos = new ArrayList<>();
        ArrayList<No> nosVisitados = new ArrayList<>();
        filaExpandidos.add(noInicial);

        while (!filaExpandidos.isEmpty()) {

            No verAtual = filaExpandidos.remove(0);
            imprimir(verAtual.getEstado());
            int posicaoDoZero = buscarZero(verAtual.getEstado());
            if (Arrays.equals(verAtual.getEstado(), estadoSolucao)) {
                System.out.println("");
                System.out.println("___SOLUÇÃO ENCONTRADA___");
                return verAtual;

            }

            ArrayList<No> filhos = new ArrayList<>();
            nosVisitados.add(verAtual);
            switch (posicaoDoZero) {

                case 0:
                    //if index 0 pode mover para index 1 e 3
                    int[] mat = gerarCopia(verAtual.getEstado());
                    No filho = new No(mat);
                    filho.getEstado()[0] = filho.getEstado()[1];
                    filho.getEstado()[1] = 0;

                    if (!nosConhecidos(nosVisitados, filho)) {
                        filaExpandidos.add(filho);
                    }
                    filhos.add(filho);

                    int[] mat2 = gerarCopia(verAtual.getEstado());
                    No filho2 = new No(mat2);
                    filho2.getEstado()[0] = filho2.getEstado()[3];
                    filho2.getEstado()[3] = 0;

                    if (!nosConhecidos(nosVisitados, filho2)) {
                        filaExpandidos.add(filho2);
                    }
                    filhos.add(filho2);

                    break;

                case 1:
                    //if index 1 pode mover para index 0, 2 e 4
                    int[] mat3 = gerarCopia(verAtual.getEstado());
                    No filho3 = new No(mat3);
                    filho3.getEstado()[1] = filho3.getEstado()[0];
                    filho3.getEstado()[0] = 0;

                    if (!nosConhecidos(nosVisitados, filho3)) {
                        filaExpandidos.add(filho3);
                    }
                    filhos.add(filho3);

                    int[] mat4 = gerarCopia(verAtual.getEstado());
                    No filho4 = new No(mat4);
                    filho4.getEstado()[1] = filho4.getEstado()[2];
                    filho4.getEstado()[2] = 0;

                    if (!nosConhecidos(nosVisitados, filho4)) {
                        filaExpandidos.add(filho4);

                    }
                    filhos.add(filho4);

                    int[] mat5 = gerarCopia(verAtual.getEstado());
                    No filho5 = new No(mat5);
                    filho5.getEstado()[1] = filho5.getEstado()[4];
                    filho5.getEstado()[4] = 0;

                    if (!nosConhecidos(nosVisitados, filho5)) {
                        filaExpandidos.add(filho5);

                    }
                    filhos.add(filho5);
                    break;
                case 2:
                    // if index 2 pode mover para index 1 e 5
                    int[] mat6 = gerarCopia(verAtual.getEstado());
                    No filho6 = new No(mat6);
                    filho6.getEstado()[2] = filho6.getEstado()[1];
                    filho6.getEstado()[1] = 0;

                    if (!nosConhecidos(nosVisitados, filho6)) {
                        filaExpandidos.add(filho6);
                    }
                    filhos.add(filho6);

                    int[] mat7 = gerarCopia(verAtual.getEstado());
                    No filho7 = new No(mat7);
                    filho7.getEstado()[2] = filho7.getEstado()[5];
                    filho7.getEstado()[5] = 0;

                    if (!nosConhecidos(nosVisitados, filho7)) {
                        filaExpandidos.add(filho7);
                    }
                    filhos.add(filho7);

                    break;

                case 3:
                    //if index 3 pode mover para index 0, 4, 6
                    int[] mat8 = gerarCopia(verAtual.getEstado());
                    No filho8 = new No(mat8);
                    filho8.getEstado()[3] = filho8.getEstado()[0];
                    filho8.getEstado()[0] = 0;

                    if (!nosConhecidos(nosVisitados, filho8)) {
                        filaExpandidos.add(filho8);
                    }

                    filhos.add(filho8);
                    int[] mat9 = gerarCopia(verAtual.getEstado());
                    No filho9 = new No(mat9);
                    filho9.getEstado()[3] = filho9.getEstado()[4];
                    filho9.getEstado()[4] = 0;

                    if (!nosConhecidos(nosVisitados, filho9)) {
                        filaExpandidos.add(filho9);

                    }
                    filhos.add(filho9);

                    int[] mat10 = gerarCopia(verAtual.getEstado());
                    No filho10 = new No(mat10);
                    filho10.getEstado()[3] = filho10.getEstado()[6];
                    filho10.getEstado()[6] = 0;

                    if (!nosConhecidos(nosVisitados, filho10)) {
                        filaExpandidos.add(filho10);

                    }
                    filhos.add(filho10);

                    break;

                case 4:
                    //if index 4 pode mover para index 1, 3, 5, 7
                    int[] mat11 = gerarCopia(verAtual.getEstado());
                    No filho11 = new No(mat11);
                    filho11.getEstado()[4] = filho11.getEstado()[1];
                    filho11.getEstado()[1] = 0;

                    if (!nosConhecidos(nosVisitados, filho11)) {
                        filaExpandidos.add(filho11);

                    }
                    filhos.add(filho11);
                    int[] mat12 = gerarCopia(verAtual.getEstado());
                    No filho12 = new No(mat12);
                    filho12.getEstado()[4] = filho12.getEstado()[3];
                    filho12.getEstado()[3] = 0;

                    if (!nosConhecidos(nosVisitados, filho12)) {
                        filaExpandidos.add(filho12);

                    }
                    filhos.add(filho12);
                    int[] mat13 = gerarCopia(verAtual.getEstado());
                    No filho13 = new No(mat13);
                    filho13.getEstado()[4] = filho13.getEstado()[5];
                    filho13.getEstado()[5] = 0;

                    if (!nosConhecidos(nosVisitados, filho13)) {
                        filaExpandidos.add(filho13);

                    }
                    filhos.add(filho13);
                    int[] mat14 = gerarCopia(verAtual.getEstado());
                    No filho14 = new No(mat14);
                    filho14.getEstado()[4] = filho14.getEstado()[7];
                    filho14.getEstado()[7] = 0;

                    if (!nosConhecidos(nosVisitados, filho14)) {
                        filaExpandidos.add(filho14);

                    }
                    filhos.add(filho14);

                    break;

                case 5:
                    //if index 5 pode mover para index 2, 4, 8
                    int[] mat15 = gerarCopia(verAtual.getEstado());
                    No filho15 = new No(mat15);
                    filho15.getEstado()[5] = filho15.getEstado()[2];
                    filho15.getEstado()[2] = 0;

                    if (!nosConhecidos(nosVisitados, filho15)) {
                        filaExpandidos.add(filho15);
                    }
                    filhos.add(filho15);

                    int[] mat16 = gerarCopia(verAtual.getEstado());
                    No filho16 = new No(mat16);
                    filho16.getEstado()[5] = filho16.getEstado()[4];
                    filho16.getEstado()[4] = 0;

                    if (!nosConhecidos(nosVisitados, filho16)) {
                        filaExpandidos.add(filho16);

                    }
                    filhos.add(filho16);
                    int[] mat17 = gerarCopia(verAtual.getEstado());
                    No filho17 = new No(mat17);
                    filho17.getEstado()[5] = filho17.getEstado()[8];
                    filho17.getEstado()[8] = 0;

                    if (!nosConhecidos(nosVisitados, filho17)) {
                        filaExpandidos.add(filho17);

                    }
                    filhos.add(filho17);

                    break;

                case 6:
                    // if index 6 pode mover para index 3 e 7
                    int[] mat18 = gerarCopia(verAtual.getEstado());
                    No filho18 = new No(mat18);
                    filho18.getEstado()[6] = filho18.getEstado()[3];
                    filho18.getEstado()[3] = 0;

                    if (!nosConhecidos(nosVisitados, filho18)) {
                        filaExpandidos.add(filho18);

                    }
                    filhos.add(filho18);

                    int[] mat19 = gerarCopia(verAtual.getEstado());
                    No filho19 = new No(mat19);
                    filho19.getEstado()[6] = filho19.getEstado()[7];
                    filho19.getEstado()[7] = 0;

                    if (!nosConhecidos(nosVisitados, filho19)) {
                        filaExpandidos.add(filho19);

                    }
                    filhos.add(filho19);
                    break;
                case 7:
                    //if index 7 pode mover para index 4, 6, 8
                    int[] mat20 = gerarCopia(verAtual.getEstado());
                    No filho20 = new No(mat20);
                    filho20.getEstado()[7] = filho20.getEstado()[4];
                    filho20.getEstado()[4] = 0;

                    if (!nosConhecidos(nosVisitados, filho20)) {
                        filaExpandidos.add(filho20);

                    }
                    filhos.add(filho20);
                    int[] mat21 = gerarCopia(verAtual.getEstado());
                    No filho21 = new No(mat21);
                    filho21.getEstado()[7] = filho21.getEstado()[6];
                    filho21.getEstado()[6] = 0;

                    if (!nosConhecidos(nosVisitados, filho21)) {
                        filaExpandidos.add(filho21);

                    }
                    filhos.add(filho21);

                    int[] mat22 = gerarCopia(verAtual.getEstado());
                    No filho22 = new No(mat22);
                    filho22.getEstado()[7] = filho22.getEstado()[8];
                    filho22.getEstado()[8] = 0;

                    if (!nosConhecidos(nosVisitados, filho22)) {
                        filaExpandidos.add(filho22);

                    }
                    filhos.add(filho22);
                    break;

                case 8:
                    //if index 8 pode mover para index 5 e 7
                    int[] mat23 = gerarCopia(verAtual.getEstado());
                    No filho23 = new No(mat23);
                    filho23.getEstado()[8] = filho23.getEstado()[5];
                    filho23.getEstado()[5] = 0;

                    if (!nosConhecidos(nosVisitados, filho23)) {
                        filaExpandidos.add(filho23);
                    }
                    filhos.add(filho23);
                    int[] mat24 = gerarCopia(verAtual.getEstado());
                    No filho24 = new No(mat24);
                    filho24.getEstado()[8] = filho24.getEstado()[7];
                    filho24.getEstado()[7] = 0;

                    if (!nosConhecidos(nosVisitados, filho24)) {
                        filaExpandidos.add(filho24);
                    }
                    filhos.add(filho24);

                    break;

            }
            verAtual.setFilhos(filhos);
        }
        return null;

    }

    public static void imprimir(int[] arrayAtual) {
        for (int i = 0; i < arrayAtual.length; i++) {
            System.out.print("|" + arrayAtual[i] + "|");
        }
        System.out.println("");
    }

    public static int buscarZero(int[] arrayAtual) {
        int posZero = 0;
        for (int i = 0; i < arrayAtual.length; i++) {
            if (arrayAtual[i] == 0) {
                posZero = i;
            }

        }
        return posZero;
    }

    private static int[] gerarCopia(int[] estado) {
        int[] copia = new int[estado.length];
        System.arraycopy(estado, 0, copia, 0, estado.length);
        return copia;
    }

    private static boolean nosConhecidos(ArrayList<No> nosVisitados, No filho) {
        for (No vis : nosVisitados) {

            if (Arrays.equals(vis.getEstado(), filho.getEstado())) {
                return true;

            }

        }
        return false;
    }

}
