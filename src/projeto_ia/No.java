package projeto_ia;

import java.util.ArrayList;

public class No {

    int[] estado;
    No pai;
    ArrayList<No> filhos = new ArrayList<>();

    public No(int[] estado) {
        this.estado = estado;
        pai = null;
        filhos = null;
    }

    public int[] getEstado() {
        return estado;
    }

    public void setEstado(int[] estado) {
        this.estado = estado;
    }

    public No getPai() {
        return pai;
    }

    public void setPai(No pai) {
        this.pai = pai;
    }

    public ArrayList<No> getFilhos() {
        return filhos;
    }

     public void setFilhos(ArrayList<No> filhos) {
        this.filhos = filhos;
        if(filhos != null){
        for(No filho : filhos){
            filho.pai = this;
        
        }
        }
    }

}
